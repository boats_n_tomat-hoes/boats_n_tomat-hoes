// constants
const int VINE_LENGTH = 330;
const int EMPTY_TENDRIL_SIZE = 22;
const int CHERRY_TOM_SIZE = 35;
const int PICK_DURATION = 1500;

// enum classes
enum class TomatoSize { CHERRY, FULL };
enum class WhichVine { V1 = 1, V2 = 2 };
typedef TomatoSize TS;
typedef WhichVine V;

// classes
struct Tomato {
	bool exists;
	TomatoSize size;
};

// prototypes
void deliverTomato();
Tomato harvestVine(WhichVine whichVine);
