#include <iostream>
#include <stdexcept>
#include "com.h"
#include "nav.h"
#include "tom.h"

using namespace std;

void validatePosition();
void alignRobotForDelivery();

void deliverTomato() {
	validatePosition();
	alignRobotForDelivery();
	Actuator::trigger();
	Actuator::trigger();
	Actuator::trigger();
}

void validatePosition() {
	if(!((Position::junction() == 6) || (Position::junction() == 7))) {
		throw runtime_error("Robot needs to be at junction 6 or 7 in order for tomato delivery to take place. "
		                    "Currently the robot is a junction " + to_string(Position::junction()) + ".");
	}
}

void alignRobotForDelivery() {
	if(!(Position::atJunction() && (Position::heading() == H::EAST))) {
		adjustHeading(H::WEST);
		returnToJunction();
		LineFollower().distanceFromJunction(170)
		              .toJunction(false)
		              .follow();
	}
}
