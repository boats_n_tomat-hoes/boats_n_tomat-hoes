#include <iostream>
#include <stdexcept>
#include <stopwatch.h>
#include "com.h"
#include "nav.h"
#include "tom.h"

using namespace std;

// prototypes
void validatePosition(WhichVine whichVine);
void alignRobotForHarvesting();
void pickTomato();
void investigateTomato();
bool tomatoAbove();
void pickTomato();

// static variables/objects
Tomato tomato;
static bool Vine1Harvested = false;
static bool Vine2Harvested = false;

Tomato harvestVine(WhichVine whichVine) {
	validatePosition(whichVine);
	tomato.exists = false;
	bool *harvested = (whichVine == V::V1) ? &Vine1Harvested : &Vine2Harvested;
	if(*harvested) { return tomato; }
	alignRobotForHarvesting();
	LineFollower().distanceFromJunction(-40)
	              .reverse();
	while(Position::distanceTravelled() < VINE_LENGTH) {
		LineFollower().speed(50)
					  .distanceFromJunction(VINE_LENGTH)
					  .toJunction(false)
					  .checkForTomato()
					  .follow();
		investigateTomato();
		if(tomato.exists) {
			pickTomato();
			return tomato;
		}
	}
	(*harvested) = true;
	return tomato;
}

void validatePosition(WhichVine whichVine) {
	int int_vine = static_cast<int>(whichVine);
	if(Position::junction() != int_vine) {
		string str_vine = to_string(int_vine);
		throw runtime_error("Vine Harvesting of vine " + str_vine
		    + " requires the robot to be at junction " + str_vine + ".");
	}
}

void alignRobotForHarvesting() {
	if(!(Position::atJunction() && (Position::heading() == H::WEST))) {
		adjustHeading(H::EAST);
		returnToJunction();
	}
}

void investigateTomato() {
	int distTravld = Position::distanceTravelled();
	LineFollower().distanceFromJunction(distTravld + EMPTY_TENDRIL_SIZE)
	              .speed(50)
	              .toJunction(false)
	              .follow();
	if(!tomatoAbove()) {
		tomato.exists = false;
		cout << "no tomato" << endl;
		return;
	} else {
		tomato.exists = true;
		LEDs::red();
	}
	LineFollower().distanceFromJunction(distTravld + CHERRY_TOM_SIZE)
	              .speed(50)
	              .toJunction(false)
	              .follow();
	if(tomatoAbove()) {
		tomato.size = TS::FULL;
		LEDs::full();
		cout << "full" << endl;
	} else {
		tomato.size = TS::CHERRY;
		LEDs::cherry();
		cout << "cherry" << endl;
	}
}

bool tomatoAbove() {
	if(distanceSensor() <= TOMATO_HEIGHT) { return true; }
	else { return false; }
}

void pickTomato() {
	stopwatch watch;
	watch.start();
	while(watch.read() < PICK_DURATION) {
		Motors::tomato(-100);
	}
	watch.start();
	while(watch.read() < PICK_DURATION) {
		Motors::tomato(100);
	}
}
