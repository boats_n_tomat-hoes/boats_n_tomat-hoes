#include "nav.h"

// prototypes
Headings oppositeHeading(Headings heading);
void insertTrip(Trip trips[][9], int i, int j, short int length, Headings head);

Trip Trips::trips[9][9];

void Trips::initialize() {
	// initialize top right half of matrix with invalid trips
	for(int i = 0; i < 9; i++) {
		for(int j = 0; j < 9; j++) {
			if(i <= j) {
				trips[i][j].valid = false;
			}
		}
	}
	// insert data in top right half of matrix
	insertTrip(trips, 0, 1,  726, H::WEST);
	insertTrip(trips, 1, 2,  567, H::WEST);
	insertTrip(trips, 1, 5,  480, H::SOUTH);
	insertTrip(trips, 2, 3,  299, H::WEST);
	insertTrip(trips, 3, 4,  480, H::SOUTH);
	insertTrip(trips, 4, 5,  865, H::EAST);
	insertTrip(trips, 5, 6, 1269, H::SOUTH);
	insertTrip(trips, 6, 7,  590, H::WEST);
	insertTrip(trips, 6, 8,  914, H::EAST);
	// populate bottom left half of matrix automatically
	for(int i = 0; i < 9; i++) {
		for(int j = 0; j < 9; j++) {
			if(i > j) {
				trips[i][j].valid    = trips[j][i].valid;
				trips[i][j].length   = trips[j][i].length;
				trips[i][j].heading  = oppositeHeading(trips[j][i].heading);
			}
		}
	}
}

Trip Trips::tripFromTo(int origin, int destination) {
	return trips[origin][destination];
}

Headings oppositeHeading(Headings heading) {
	return !heading;
}

void insertTrip(Trip trips[][9], int i, int j, short int length, Headings head) {
	trips[i][j].valid = true;
	trips[i][j].length = length;
	trips[i][j].heading = head;
}

/* Map
       |              |                                       |      
       |              |                                       |      
   ---------          |                                       |      
   |   |   |          |                  |                    |      
   |   |   |          |                  |                    |      
-----------------------------------------|--------------------8------
   |   |   |          |                  |                    |      
   |   |   |          |                  |                    |      
   ----0----          |                                       |      
       |              |                                       |      
       |              |                                      914     
      726             |                 []                    |      
       |              |                 []                    |      
       |              |                                       |      
-------1-----480------5-----------------1269------------------6------
       |              |                                       |      
       |              |                 []                    |      
      567             |                 []                    |      
       |              |                                      590     
       |             865                                      |      
       |              |                                       |      
-------2-------       |                  E                    |      
       |              |                  |              ------7------
      299             |               N--o--S                 |      
       |              |                  |                    |      
-------3-----480------4---------         W                    |      
       |                                                      |      
       |                                                      |      
       |                                                      |      
       |                                                      |      
*/
