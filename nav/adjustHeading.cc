#include <iostream>
#include <stdexcept>
#include "nav.h"

using namespace std;

void validateTurn(Headings currentHeading, Headings desiredHeading);

void adjustHeading(Headings desiredHeading) {
	Headings currentHeading = Position::heading();
	validateTurn(currentHeading, desiredHeading);
	cout << "desired heading: " << static_cast<int>(desiredHeading) << endl;
	cout << "current heading: " << static_cast<int>(currentHeading) << endl;
	if(desiredHeading == ++currentHeading) { turnRight(); return; }
	if(desiredHeading == --currentHeading) { turnLeft();  return; }
	if(desiredHeading ==  !currentHeading) { turn180();   return; }
}

void validateTurn(Headings currentHeading, Headings desiredHeading) {
	if(!Position::atJunction() && (desiredHeading != currentHeading)) {
		throw runtime_error("Robot is currently not at a junction, hence heading can't be changed.");
	}
}
