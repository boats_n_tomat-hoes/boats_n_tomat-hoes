#include "nav.h"

Headings operator++(Headings heading) {
	int headingInDeg = static_cast<int>(heading);
	headingInDeg += 90;
	if(headingInDeg >= 360) { headingInDeg -= 360; }
	return static_cast<Headings>(headingInDeg);
}

Headings operator--(Headings heading) {
	int headingInDeg = static_cast<int>(heading);
	headingInDeg -= 90;
	if(headingInDeg < 0) { headingInDeg += 360; }
	return static_cast<Headings>(headingInDeg);
}

Headings operator!(Headings heading) {
	int headingInDeg = static_cast<int>(heading);
	headingInDeg += 180;
	if(headingInDeg >= 360) { headingInDeg -= 360; }
	return static_cast<Headings>(headingInDeg);
}
