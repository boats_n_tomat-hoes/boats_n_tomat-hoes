#include <iostream>
#include <iomanip>
#include <math.h>
#include <stopwatch.h>
#include "nav.h"
#include "com.h"

using namespace std;

// enum classes
enum class LineLocations { RIGHT, MIDDLE, LEFT };
typedef LineLocations L;

// prototypes for follow() function
void updateDistanceTravelled();
bool junctionDetected(LightSensors lightSensors);
void recoverFromDarkness(LineLocations lineLocation);
void recoverFromOvershoot(int speed);
bool robotAtLowHangingTomatoes();

// static variables/objects
static LineLocations lineLocation = L::MIDDLE;
static stopwatch watch;
static int timestamp;

LineFollower::LineFollower() {
	// set default values
	_speed = 100;
	_distanceFromJunction = 0;
	_toJunction = true;
	_checkForTomato = false;
}

LineFollower &LineFollower::speed(int mySpeed) {
	this->_speed = mySpeed;
	return *this;
}

LineFollower &LineFollower::distanceFromJunction(int distance) {
	this->_distanceFromJunction = distance;
	return *this;
}

LineFollower &LineFollower::toJunction(bool myBool) {
	this->_toJunction = myBool;
	return *this;
}

LineFollower &LineFollower::checkForTomato() {
	this->_checkForTomato = true;
	return *this;
}

void LineFollower::follow() {
	int fast = this->_speed;
	float correctionFactor = (this->_speed / 100.0) * (LINE_FOLL_CORR_FCT_FAST - LINE_FOLL_CORR_FCT_SLOW) + LINE_FOLL_CORR_FCT_SLOW;
	int slow = floor(correctionFactor * fast);
	
	watch.start();
	timestamp = watch.read();
	while(true) {
		// line following
		LightSensors lightSensors;
		
		if((lightSensors.left() == C::WHITE) && (lightSensors.right() == C::WHITE)) {
			Motors::both(fast);
			lineLocation = L::MIDDLE;
		}
		if((lightSensors.left() == C::WHITE) && (lightSensors.right() == C::BLACK)) {
			Motors::left(slow);
			Motors::right(fast);
			lineLocation = L::LEFT;
		}
		if((lightSensors.left() == C::BLACK) && (lightSensors.right() == C::WHITE)) {
			Motors::left(fast);
			Motors::right(slow);
			lineLocation = L::RIGHT;
		}
		if((lightSensors.left() == C::BLACK) && (lightSensors.right() == C::BLACK)) {
			recoverFromDarkness(lineLocation);
		}
		// update Position::_distanceTravelled
		updateDistanceTravelled();
		// loop control statements
		// (1) follow line to next junction
		int maxJunctnDist = floor(UPPER_DIST_ACCURACY * _distanceFromJunction + DIST_TOLERANCE);
		int minJunctnDist = floor(LOWER_DIST_ACCURACY * _distanceFromJunction - DIST_TOLERANCE);
		bool nearJunction = ((Position::distanceTravelled() >= minJunctnDist) && (Position::distanceTravelled() <= maxJunctnDist));
		if((this->_toJunction == true) && nearJunction) {
			if(junctionDetected(lightSensors) == true) {
				cout << "Junction detected!" << endl;
				cout << "distanceTravelled: " << Position::distanceTravelled() << endl;
				Position::updateDistanceTravelled(0.0);
				break;
			}
		}
		if(Position::distanceTravelled() > maxJunctnDist) {
			cout << "distanceTravelled: " << Position::distanceTravelled() << endl;
			recoverFromOvershoot(this->_speed);
			Position::updateDistanceTravelled(0.0);
			break;
		}
		// (2) follow line for set distance
		if((this->_toJunction == false) && (Position::distanceTravelled() >= this->_distanceFromJunction)) {
			break;
		}
		// (3) follow line until tomato is discovered (or for set distance)
		if(this->_checkForTomato == true) {
			// uglyl fix I had to implement last minute due to robot now being able to pick low hanging tomatoes
			if(!robotAtLowHangingTomatoes()) {
				if(distanceSensor() <= TOMATO_HEIGHT) {
					break;
				}
			}
		}
	}
	Motors::both(0);
}

void LineFollower::reverse() {
	watch.start();
	timestamp = watch.read();
	while(Position::distanceTravelled() >= _distanceFromJunction) {
		LightSensors lightSensors;
		if((lightSensors.left() == C::WHITE) && (lightSensors.right() == C::BLACK)) {
			lineLocation = L::LEFT;
		}
		if((lightSensors.left() == C::BLACK) && (lightSensors.right() == C::WHITE)) {
			lineLocation = L::RIGHT;
		}
		Motors::both(-this->_speed);
		updateDistanceTravelled();
	}
	Motors::both(0);
}

void updateDistanceTravelled() {
	int elapsedTime = watch.read() - timestamp;
	if((elapsedTime) >= 50) {
		float currentSpeed = 0.5 * (Motors::getLeftSpeed() + Motors::getRightSpeed());
		timestamp = watch.read();
		float distInc = SPEED_DIST_CONV_FCT * currentSpeed * elapsedTime;
		Position::incrementDistanceTravelled(distInc);
	}
}

bool junctionDetected(LightSensors lightSensors) {
	if((lightSensors.leftWing() == C::WHITE) || (lightSensors.rightWing() == C::WHITE)) {
		return true;
	}
	else {
		return false;
	}
}

void recoverFromDarkness(LineLocations lineLocation) {
	float currentSpeed = 0.5 * (Motors::getLeftSpeed() + Motors::getRightSpeed());
	int verySlow = floor(0.2 * currentSpeed);
	switch(lineLocation) {
		case L::LEFT:
			Motors::left(verySlow);
			break;
		case L::RIGHT:
			Motors::right(verySlow);
			break;
		case L::MIDDLE:
			// false alarm, do nothing
			break;
	}
}

void recoverFromOvershoot(int speed) {
	cout << "recovering from junction overshoot!" << endl;
	int halfSpeed = floor(0.5 * speed);
	while(true) {
		LightSensors lightSensors;
		Motors::both(-halfSpeed);
		if(junctionDetected(lightSensors)) { break; }
	}
}

bool robotAtLowHangingTomatoes() {
	int distTravld = Position::distanceTravelled();
	int lowerBound = AVOID_TOMATO_LOWER_DIST;
	bool atVine1 = Position::junction() == 1;
	int upperBound = atVine1 ? AVOID_TOMATO_UPPER_DIST_V1 : AVOID_TOMATO_UPPER_DIST_V2;
	if((distTravld > lowerBound) && (distTravld < upperBound)) {
		return true;
	} else {
		return false;
	}
}
