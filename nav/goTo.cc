#include <iostream>
#include <vector>
#include <stdexcept>
#include "nav.h"

using namespace std;

// prototypes
void validateTrip(Trip trip, int destination);
void alignRobotForFirstTrip(int firstDestination);
void adjustHeading(Headings desiredHeading);

// a journey consists of many trips! (or at least one)
void goTo(vector<int> journey) {
	int firstDestination = journey.front();
	alignRobotForFirstTrip(firstDestination);
	while(!journey.empty()) {
		int destination = journey.front();
		Trip trip = Trips::tripFromTo(Position::junction(), destination);
		validateTrip(trip, destination);
		adjustHeading(trip.heading);
		LineFollower().distanceFromJunction(trip.length)
		              .follow();
		cout << "I was supposed to travel a distance of " << trip.length << endl;
		Position::updateJunction(destination);
		cout << "I'm now at junction " << Position::junction() << endl;
		journey.erase(journey.begin());
	}
}

void validateTrip(Trip trip, int destination) {
	if(!trip.valid) {
		throw runtime_error(string("Trip from ") + to_string(Position::junction())
						                + " to " + to_string(destination) + " is invalid!");
	}
}

void alignRobotForFirstTrip(int firstDestination) {
	Trip firstTrip = Trips::tripFromTo(Position::junction(), firstDestination);
	validateTrip(firstTrip, firstDestination);
	if(!Position::atJunction() && (Position::heading() != firstTrip.heading)) { returnToJunction(); }
}
