#include <vector>

using namespace std;

// constants needed in LineFollower
const float LINE_FOLL_CORR_FCT_FAST = 0.75;
const float LINE_FOLL_CORR_FCT_SLOW = 0.30;
const float SPEED_DIST_CONV_FCT = 0.000895; // increase if robot goes too far
const float LOWER_DIST_ACCURACY = 0.8;
const float UPPER_DIST_ACCURACY = 1.2;
const int DIST_TOLERANCE = 60;
const int TOMATO_HEIGHT = 135;

const int AVOID_TOMATO_LOWER_DIST = 50;
const int AVOID_TOMATO_UPPER_DIST_V1 = 180;
const int AVOID_TOMATO_UPPER_DIST_V2 = 125;

// constants needed in turn-functions
const float FRICTIONAL_TORQUE_CONST_90  = 0.820;
const float FRICTIONAL_TORQUE_CONST_180 = 0.354;
const int HALF_AXIS = 68;
const int ROBOT_LENGTH = 202; // distance between light sensors and rear axis

// enum classes
enum class Headings { NORTH = 0, EAST = 90, SOUTH = 180, WEST = 270 };
Headings operator++(Headings heading); // increases heading by 90 deg
Headings operator--(Headings heading); // decreases heading by 90 deg
Headings operator! (Headings heading); // changes heading by 180 deg
typedef Headings H;

// classes
class LineFollower {
	private:
		int _speed;
		int _distanceFromJunction;
		bool _toJunction;
		bool _checkForTomato;
	public:
		LineFollower();
		LineFollower &speed(int);
		LineFollower &distanceFromJunction(int);
		LineFollower &toJunction(bool);
		LineFollower &checkForTomato();
		void follow();
		void reverse();
};
class Position {
	private:
		static int _junction;
		static Headings _heading;
		static double _distanceTravelled;
		// disallow creating an instance of the class
		Position();
	public:
		static void initialize();
		static int junction();
		static Headings heading();
		static float distanceTravelled();
		static void updateJunction(int junc);
		static void updateHeading(Headings heading);
		static void updateDistanceTravelled(int dist);
		static void incrementDistanceTravelled(float inc);
		static bool atJunction();
};
struct Trip {
	bool valid;
	short int length;
	Headings heading;
};
class Trips {
	private:
		static Trip trips[9][9];
		// disallow creating an instance of the class
		Trips();
	public:
		static void initialize();
		static Trip tripFromTo(int origin, int destination);
};

// prototypes
void turnLeft();
void turnRight();
void turn180();
void goTo(vector<int> journey);
void adjustHeading(Headings desiredHeading);
void returnToJunction();
