#include <iostream>
#include <math.h>
#include <stopwatch.h>
#include "nav.h"
#include "com.h"

using namespace std;

// enum classes
enum class Directionals { LEFT, RIGHT };
typedef Directionals D;

// prototypes
void turn(bool one80, Directionals direction);
void turnLeft180();
void turnRight180();
int degreesTurned();
void recoverFromOverturn();
int degreesTurned(bool one80);
void recoverFromOverturn(bool one80);

// static variables/objects
static int speed;
static stopwatch watch;
static WhichMotor motor;
static WhichMotor otherMotor;
static WhichSensor wing;
static WhichSensor center;
static WhichSensor centerOpposite;
static float wheelTravel = 0.0;
static int timestamp;

void turnLeft() {
	turn(false, D::LEFT);
	Position::updateHeading(--Position::heading());
	Position::updateDistanceTravelled(0);
	Position::incrementDistanceTravelled((float)(ROBOT_LENGTH + HALF_AXIS));
}
void turnRight() {
	turn(false, D::RIGHT);
	Position::updateHeading(++Position::heading());
	Position::updateDistanceTravelled(0);
	Position::incrementDistanceTravelled((float)(ROBOT_LENGTH + HALF_AXIS));
}
void turn180() {
	float distanceAlreadyTravelled = Position::distanceTravelled();
	switch(Position::junction()) {
		case 0: case 1: case 2: case 3:
			switch(Position::heading()) {
				case H::WEST: turnLeft180();  break;
				case H::EAST: turnRight180(); break;
				default: turnRight180();
			}
			break;
		case 6: case 7: case 8:
			switch(Position::heading()) {
				case H::WEST: turnRight180(); break;
				case H::EAST: turnLeft180();  break;
				default: turnRight180();
			}
			break;
		default: turnRight180();
	}
	Position::updateHeading(!Position::heading());
	Position::updateDistanceTravelled(-1.0 * distanceAlreadyTravelled);
	Position::incrementDistanceTravelled(2 * ROBOT_LENGTH);
}
void turnLeft180() {
	turn(true, D::LEFT);
}
void turnRight180() {
	turn(true, D::RIGHT);
}

void turn(bool one80, Directionals direction) {
	speed = 100;
	if(one80) { speed = floor(0.8 * speed); }
	int slowSpeed = floor(0.7 * speed);
	
	int forwardTravel = ROBOT_LENGTH - HALF_AXIS;
	bool pointingAtVine = (((Position::junction() == 1) || (Position::junction() == 2)) && (Position::heading() == H::NORTH));
	if(pointingAtVine) { forwardTravel -= 40; }
	if(one80 == false) {
		LineFollower().distanceFromJunction(forwardTravel)
		              .toJunction(false)
		              .follow();
	}
	
	switch(direction) {
		case D::LEFT:
			wing = S::LEFT_WING;
			center = S::LEFT;
			centerOpposite = S::RIGHT;
			break;
		case D::RIGHT:
			wing = S::RIGHT_WING;
			center = S::RIGHT;
			centerOpposite = S::LEFT;
			break;
	}
	
	int minTurnAngle1 = one80 ? 120 : 30;
	int minTurnAngle2 = one80 ? 135 : 40;
	int maxTurnAngle1 = one80 ? 240 : 115;
	int maxTurnAngle2 = one80 ? 280 : 130;
	
	motor      = (direction == D::LEFT) ? M::RIGHT : M::LEFT;
	otherMotor = (direction == D::LEFT) ? M::LEFT  : M::RIGHT;
	
	watch.start();
	wheelTravel = 0.0;
	timestamp = watch.read();
	while((lightSensor(wing) != C::WHITE) || (degreesTurned(one80) < minTurnAngle1)) {
		Motors::run(motor, speed);
		if(one80) { Motors::run(otherMotor, -speed); }
		if(degreesTurned(one80) > maxTurnAngle1) {
			recoverFromOverturn(one80);
			return;
		}
	}
	while((lightSensor(centerOpposite) != C::WHITE) || (degreesTurned(one80) < minTurnAngle2)) {
		Motors::run(motor, slowSpeed);
		if(one80) { Motors::run(otherMotor, -slowSpeed); }
		if(degreesTurned(one80) > maxTurnAngle2) {
			recoverFromOverturn(one80);
			return;
		}
	}
	Motors::both(0);
}

int degreesTurned(bool one80) {
	int elapsedTime = watch.read() - timestamp;
	int currentSpeed = Motors::getSpeed(motor);
	if(elapsedTime > 30) {
		timestamp = watch.read();
		float frictionConstant = (one80)? FRICTIONAL_TORQUE_CONST_180 : FRICTIONAL_TORQUE_CONST_90;
		wheelTravel += frictionConstant * SPEED_DIST_CONV_FCT * currentSpeed * elapsedTime;
	}
	int radius = one80 ? HALF_AXIS : (2 * HALF_AXIS);
	int degreesTurned = floor(wheelTravel * ( 360.0 / (2.0 * M_PI) ) / (float)radius);
	if(one80) { degreesTurned *= 2; }
	return degreesTurned;
}

void recoverFromOverturn(bool one80) {
	int halfSpeed = one80 ? floor(0.5 * speed) : speed;
	while(lightSensor(center) != C::WHITE) {
		Motors::run(motor, -halfSpeed);
		if(one80) { Motors::run(otherMotor, +halfSpeed); }
	}
}
