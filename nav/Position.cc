#include <iostream>
#include <math.h>
#include "nav.h"

int Position::_junction;

Headings Position::_heading;

double Position::_distanceTravelled;

void Position::initialize() {
	_junction = 0;
	_heading = H::WEST;
	_distanceTravelled = 0.0;
}

int Position::junction() {
	return _junction;
}

Headings Position::heading() {
	return _heading;
}

float Position::distanceTravelled() {
	return _distanceTravelled;
}

void Position::updateJunction(int junc) {
	_junction = junc;
}

void Position::updateHeading(Headings heading) {
	_heading = heading;
}

void Position::updateDistanceTravelled(int dist) {
	_distanceTravelled = dist;
}

void Position::incrementDistanceTravelled(float inc) {
	_distanceTravelled += inc;
}

bool Position::atJunction() {
	bool atJunction = (floor(_distanceTravelled) == 0);
	return atJunction;
}
