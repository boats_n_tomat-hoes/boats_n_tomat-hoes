#include "nav.h"

void returnToJunction() {
	LineFollower().distanceFromJunction(2 * ROBOT_LENGTH + 100)
				  .toJunction(false)
				  .follow();
	turn180();
	LineFollower().distanceFromJunction(0)
				  .toJunction(true)
				  .follow();
}
