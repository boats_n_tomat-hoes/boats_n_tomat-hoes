# Main Makefile

main :  main.o com/Motors.o com/LightSensors.o com/initializeConnection.o com/distanceSensor.o com/Actuator.o com/LEDs.o nav/LineFollower.o nav/turn.o nav/Position.o nav/Headings.o nav/Trips.o nav/goTo.o nav/adjustHeading.o nav/returnToJunction.o tom/deliverTomato.o tom/harvestVine.o
	g++ -L/export/teach/1BRobot -o main  main.o  com/Motors.o  com/LightSensors.o  com/initializeConnection.o  com/distanceSensor.o com/Actuator.o com/LEDs.o nav/LineFollower.o  nav/turn.o  nav/Position.o  nav/Headings.o  nav/Trips.o  nav/goTo.o  nav/adjustHeading.o  nav/returnToJunction.o  tom/deliverTomato.o  tom/harvestVine.o  -lrobot

main.o: main.cc  com/com.h nav/nav.h tom/tom.h
	g++ -Icom -Inav -Itom -std=c++11 -Wall -g -I/export/teach/1BRobot -c main.cc -omain.o

com/Motors.o: com/Motors.cc
	cd com && $(MAKE)
com/LightSensors.o: com/LightSensors.cc
	cd com && $(MAKE)
com/initializeConnection.o: com/initializeConnection.cc
	cd com && $(MAKE)
com/distanceSensor.o: com/distanceSensor.cc
	cd com && $(MAKE)
com/Actuator.o: com/Actuator.cc
	cd com && $(MAKE)
com/LEDs.o: com/LEDs.cc
	cd com && $(MAKE)
nav/LineFollower.o: nav/LineFollower.cc
	cd nav && $(MAKE)
nav/turn.o: nav/turn.cc
	cd nav && $(MAKE)
nav/Position.o: nav/Position.cc
	cd nav && $(MAKE)
nav/Headings.o: nav/Headings.cc
	cd nav && $(MAKE)
nav/Trips.o: nav/Trips.cc
	cd nav && $(MAKE)
nav/goTo.o: nav/goTo.cc
	cd nav && $(MAKE)
nav/adjustHeading.o: nav/adjustHeading.cc
	cd nav && $(MAKE)
nav/returnToJunction.o: nav/returnToJunction.cc
	cd nav && $(MAKE)
tom/deliverTomato.o: tom/deliverTomato.cc
	cd tom && $(MAKE)
tom/harvestVine.o: tom/harvestVine.cc
	cd tom && $(MAKE)
