#include <iostream>
#include <stdlib.h>
#include <robot_instr.h>
#include <robot_link.h>
#include "com.h"
#define ROBOT_NUM 7

using namespace std;

robot_link rlink;

int initializeConnection() {
	int val;                            // data from microprocessor
	if(!rlink.initialise(ROBOT_NUM)) {
		cout << "Cannot initialise link" << endl;
		rlink.print_errs("  ");
		return -1;
	}
	val = rlink.request(TEST_INSTRUCTION); // send test instruction
	if(val == TEST_INSTRUCTION_RESULT) { 
		cout << "Connection successfully initialized" << endl;
		return 1;
	}
	else {
		cout << "Test failed: ";
		if(val == REQUEST_ERROR) {
			cout << "Fatal errors on link: " << endl;
			rlink.print_errs();
		}
		return -1;
	}
}
