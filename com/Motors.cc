#include <robot_instr.h>
#include <iostream>
#include <stdlib.h>
#include <math.h>
#include "com.h"

using namespace std;

//prototypes
float convertSpeedToPower(WhichMotor whichOne, int speed);
int convertPowerToChar(float power);

int Motors::leftMotorSpeed;

int Motors::rightMotorSpeed;

int Motors::tomatoMotorSpeed;

void Motors::run(WhichMotor whichOne, int speed) {
	if(speed >  100) { speed = 100; }
	if(speed < -100) { speed = -100; }

	bool speedChanged = (speed != getSpeed(whichOne));
	float power = convertSpeedToPower(whichOne, speed);
	command_instruction instruc;
	switch(whichOne) {
		case M::LEFT:
			instruc = MOTOR_1_GO;
			leftMotorSpeed = speed;
			power *= (-1); // reverse direction
			break;
		case M::RIGHT:
			instruc = MOTOR_3_GO;
			rightMotorSpeed = speed;
			break;
		case M::TOMATO:
			instruc = MOTOR_4_GO;
			tomatoMotorSpeed = speed;
			power *= (-1); // reverse direction
			break;
	}
	int charPower = convertPowerToChar(power);
	if(speedChanged) { rlink.command(instruc, charPower); }
}

void Motors::left(int speed) {
	Motors::run(M::LEFT, speed);
}

void Motors::right(int speed) {
	Motors::run(M::RIGHT, speed);
}

void Motors::both(int speed) {
	Motors::run(M::LEFT,  speed);
	Motors::run(M::RIGHT, speed);
}

void Motors::tomato(int speed) {
	Motors::run(M::TOMATO, speed);
}

int Motors::getSpeed(WhichMotor whichMotor) {
	switch(whichMotor) {
		case M::LEFT:  return leftMotorSpeed;
		case M::RIGHT: return rightMotorSpeed;
		case M::TOMATO: return tomatoMotorSpeed;
	}
}

int Motors::getLeftSpeed() {
	return Motors::getSpeed(M::LEFT);
}

int Motors::getRightSpeed() {
	return Motors::getSpeed(M::RIGHT);
}

int Motors::getTomatoSpeed() {
	return Motors::getSpeed(M::TOMATO);
}

float convertSpeedToPower(WhichMotor whichOne, int speed) {
	float minMotorPower;
	float maxMotorPower;
	switch(whichOne) {
		case M::LEFT:
			minMotorPower = MIN_LEFT_MOTOR_POWER;
			maxMotorPower = MAX_LEFT_MOTOR_POWER;
			break;
		case M::RIGHT:
			minMotorPower = MIN_RIGHT_MOTOR_POWER;
			maxMotorPower = MAX_RIGHT_MOTOR_POWER;
			break;
		case M::TOMATO:
			minMotorPower = MIN_TOMATO_MOTOR_POWER;
			maxMotorPower = MAX_TOMATO_MOTOR_POWER;
	}
	float powerGradient = ((maxMotorPower - minMotorPower)/100.0);
	float power = (powerGradient * abs(speed) + minMotorPower);
	return (speed > 0)? power : -power;
}

int convertPowerToChar(float power) {
	int charPower = floor( (127.0/100.0) * power );
	if(charPower < 0) {
		charPower = abs(charPower) + 128;
	}
	return charPower;
}
