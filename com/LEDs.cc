#include <robot_instr.h>
#include "com.h"

void writeBit(int n);

void LEDs::cherry() {
	writeBit(6);
}
void LEDs::full() {
	writeBit(7);
}
void LEDs::red() {
	writeBit(4);
}
void LEDs::off() {
	rlink.command(WRITE_PORT_6, 208);
}

void writeBit(int n) {
	int val = rlink.request(READ_PORT_6);
	val = val bitand (~(0x01<< n));
	rlink.command(WRITE_PORT_6, val);
}
