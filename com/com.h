#include <robot_link.h>

// constants needed for Motors
const float MIN_LEFT_MOTOR_POWER = 27.0;
const float MIN_RIGHT_MOTOR_POWER = 27.0;
const float MAX_LEFT_MOTOR_POWER = 98.0;
const float MAX_RIGHT_MOTOR_POWER = 100.0;
const float MIN_TOMATO_MOTOR_POWER = 20.0;
const float MAX_TOMATO_MOTOR_POWER = 100.0;
// constants needed for distance sensor
const float IR_DIST_CONV_FCT = 12000.0;

// enum classes
enum class WhichSensor { LEFT_WING = 0, LEFT = 1, RIGHT = 2, RIGHT_WING = 3 };
enum class WhichMotor { LEFT = 1, RIGHT = 2, TOMATO };
enum class Colors { BLACK = 0, WHITE = 1 };
typedef WhichSensor S;
typedef WhichMotor M;
typedef Colors C;

// singletons
extern robot_link rlink;

// prototypes
int initializeConnection();
Colors lightSensor(WhichSensor whichOne);
int distanceSensor();

// classes
class Motors {
	private:
		static int leftMotorSpeed;
		static int rightMotorSpeed;
		static int tomatoMotorSpeed;
		// disallow creating an instance of the class
		Motors();
	public:
		static void run(WhichMotor whichMotor, int speed);
		static void left(int speed);
		static void right(int speed);
		static void both(int speed);
		static void tomato(int speed);
		static int getSpeed(WhichMotor whichMotor);
		static int getLeftSpeed();
		static int getRightSpeed();
		static int getTomatoSpeed();
};
class LightSensors {
	private:
		Colors _leftWing;
		Colors _left;
		Colors _right;
		Colors _rightWing;
	public:
		LightSensors();
		Colors leftWing();
		Colors left();
		Colors right();
		Colors rightWing();
};
class Actuator {
	public:
		static void initialize();
		static void trigger();
};
class LEDs {
	public:
		static void cherry();
		static void full();
		static void red();
		static void off();
};
