#include <robot_instr.h>
#include <math.h>
#include "com.h"

int distanceSensor() {
	int val = rlink.request(ADC3);
	int distance = floor(IR_DIST_CONV_FCT * (1.0/val));
	return distance;
}
