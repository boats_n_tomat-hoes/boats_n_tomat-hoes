#include <robot_instr.h>
#include <stopwatch.h>
#include "com.h"

void Actuator::initialize() {
	rlink.command(WRITE_PORT_7, 128);
}

void Actuator::trigger() {
	stopwatch watch;
	watch.start();
	rlink.command(WRITE_PORT_7, 0);
	while(watch.read() < 1500) {}
	rlink.command(WRITE_PORT_7, 128);
}
