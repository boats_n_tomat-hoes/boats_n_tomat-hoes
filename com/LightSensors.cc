#include <robot_instr.h>
#include <iostream>
#include "com.h"

using namespace std;

// prototypes
Colors color(int lightSensorData, int whichSensor);

LightSensors::LightSensors() {
	int val = rlink.request(READ_PORT_7);
	val = val bitand 0x0F;  // 0x0F = 0000 1111 binary
	
	int leftWing  = 0x01<< 3;
	int left      = 0x01<< 2;
	int right     = 0x01<< 1;
	int rightWing = 0x01<< 0;
	
	_leftWing  = color(val, leftWing);
	_left      = color(val, left);
	_right     = color(val, right);
	_rightWing = color(val, rightWing);
}

Colors LightSensors::leftWing() {
	return _leftWing;
}
Colors LightSensors::left() {
	return _left;
}
Colors LightSensors::right() {
	return _right;
}
Colors LightSensors::rightWing() {
	return _rightWing;
}

Colors lightSensor(WhichSensor whichSensor) {
	LightSensors lightSensors;
	switch(whichSensor) {
		case S::LEFT_WING:  return lightSensors.leftWing();
		case S::LEFT:       return lightSensors.left();
		case S::RIGHT:      return lightSensors.right();
		case S::RIGHT_WING: return lightSensors.rightWing();
	}
}

Colors color(int lightSensorData, int whichSensor) {
	if((lightSensorData bitand whichSensor) != 0) { return C::WHITE; }
	else { return C::BLACK; }
}
