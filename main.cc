#include <iostream>
#include <robot_instr.h>
#include <robot_link.h>
#include "com.h"
#include "nav.h"
#include "tom.h"

using namespace std;

// prototypes
void returnToBox();

// if you don't understand what the arguments of the goTo({...}) function stand for
// have a look in nav/Trips.cc
int main() {
	initializeConnection();
	Actuator::initialize();
	Position::initialize();
	Trips::initialize();
	LEDs::off();
	goTo({1});
	for(int tomatoesPicked = 0; tomatoesPicked < 4; tomatoesPicked++) {
		Tomato tomato = harvestVine(V::V1);
		if(!tomato.exists) {
			goTo({2});
			tomato = harvestVine(V::V2);
			goTo({1});
		}
		goTo({5, 6});
		switch(tomato.size) {
			case TS::CHERRY:
				goTo({7});
				deliverTomato();
				LEDs::off();
				goTo({6});
				break;
			case TS::FULL:
				deliverTomato();
				LEDs::off();
				break;
		}
		goTo({5, 1});
	}
	goTo({0});
	returnToBox();
	return 0;
}

void returnToBox() {
	LineFollower().distanceFromJunction(300)
	              .toJunction(false)
	              .follow();
}
